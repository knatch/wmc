export default function number (string) {
  if (!string) return string
  return string.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

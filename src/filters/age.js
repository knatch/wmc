export default function getAge (fromdate) {
  if (!fromdate) return ''
  fromdate = new Date(fromdate)

  const todate = new Date()

  let age = []
  let y = [todate.getFullYear(), fromdate.getFullYear()]
  let ydiff = y[0] - y[1]
  let m = [todate.getMonth(), fromdate.getMonth()]
  let mdiff = m[0] - m[1]
  let d = [todate.getDate(), fromdate.getDate()]
  let ddiff = d[0] - d[1]

  if (mdiff < 0 || (mdiff === 0 && ddiff < 0)) --ydiff
  if (mdiff < 0) mdiff += 12
  if (ddiff < 0) {
    fromdate.setMonth(m[1] + 1, 0)
    ddiff = fromdate.getDate() - d[1] + d[0]
    --mdiff
  }

  if (ydiff > 0) age.push(ydiff + 'Y' + (ydiff > 1 ? '' : ''))
  if (mdiff > 0) age.push(mdiff + 'M' + (mdiff > 1 ? '' : ''))
  if (ddiff > 0 && ydiff < 1) age.push(ddiff + 'D' + (ddiff > 1 ? '' : ''))
  // if (age.length > 1) age.splice(age.length - 1, 0, ' and ')

  return age.join('')
}

import AgeFilter from './age'
import NumberWithCommas from './number'

export default {
  age: AgeFilter,
  numberWithCommas: NumberWithCommas
}

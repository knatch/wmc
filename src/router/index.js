import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Login from '@/components/Login'
import PatientList from '@/components/patient/patient-list'
import PatientSummaryAll from '@/components/patient/list/patient-list-all'
import PatientSummaryDepartment from '@/components/patient/list/patient-list-department'
import PatientSummaryRejected from '@/components/patient/list/rejected'
import PatientDetail from '@/components/patient/patient-detail'
import PatientEditDetail from '@/components/patient/patient-edit'
import PatientRegistration from '@/components/patient/registration/patient-registration'

// Patient Details
import PatientDetailGeneralInformation from '@/components/patient/detail/general-information'
import PatientDetailVisitSummary from '@/components/patient/detail/visit-summary'
import PatientVisitDetail from '@/components/patient/detail/visit-detail'
import PatientDetailAdmissionHistory from '@/components/patient/detail/admission-history'
import PatientDetailInvestigation from '@/components/patient/detail/investigation'
import PrintPatient from '@/components/patient/detail/print-patient'
import PatientFormDetail from '@/components/patient/detail/form'

import StatisticsRoutes from './statistics'

import Account from '@/components/account/account.vue'
import MyInfo from '@/components/account/my-info.vue'

import FormRoutes from './forms'
import SettingsRoutes from './settings'
import AddForms from '@/components/add-forms/add-forms'

import InvoicesRoutes from './invoices'
import PaymentsRoutes from './payments'
import PrintRoutes from './print'

Vue.use(Router)

const router = new Router({
  // Remove hashbang from url
  mode: 'history',
  // define routes
  routes: [
    { path: '/', component: Hello },
    { path: '/login', component: Login },
    ...PrintRoutes,
    { path: '/patient-summary',
      component: PatientList,
      children: [
        { path: 'all', component: PatientSummaryAll },
        { path: 'department', component: PatientSummaryDepartment },
        { path: 'rejected', component: PatientSummaryRejected }
      ],
      redirect: '/patient-summary/all'
    },
    { path: '/patient-detail/:patientId',
      component: PatientDetail,
      children: [
        { path: 'general-information', component: PatientDetailGeneralInformation, name: 'General Information' },
        { path: 'visit-summary', component: PatientDetailVisitSummary, props: true, name: 'Visit Summary' },
        { path: 'visit/:visitId', component: PatientVisitDetail, props: true, name: 'Visit Detail' },
        { path: 'form/:formId', component: PatientFormDetail, props: true, name: 'Form Detail' },
        { path: 'admission-history', component: PatientDetailAdmissionHistory },
        { path: 'investigation', component: PatientDetailInvestigation },
        { path: 'print-patient', component: PrintPatient }

      ],
      redirect: '/patient-detail/:patientId/visit-summary'
    },
    { path: '/patient-detail/:patientId/edit', component: PatientEditDetail },
    { path: '/patient-registration', component: PatientRegistration },
    StatisticsRoutes,
    FormRoutes,
    SettingsRoutes,
    { path: '/charts', component: AddForms },
    { path: '/account/',
      component: Account,
      children: [
        { path: 'my-info', component: MyInfo }
      ]
    },
    InvoicesRoutes,
    PaymentsRoutes,
    { path: '*', redirect: '/' }
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

export default router

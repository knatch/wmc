import PatientRegister from '@/components/print/PatientRegister'
import MedCertOld from '@/components/print/MedCertOld'
import MedCertBlank from '@/components/print/MedCert-Blank'
import PrintMedicalTransport from '@/components/print/MedicalTransport'
import Invoice from '@/components/print/Invoice'
import Receipt from '@/components/print/Receipt'
import OpdCard from '@/components/print/OpdCard'
import MedCert from '@/components/print/MedCert'
import NursingAssessment from '@/components/print/NursingAssessment'

const meta = {
  isPrint: true
}
export default [
  {
    name: 'PrintPatientRegister',
    path: '/print/patient-register/:patientId',
    component: PatientRegister,
    meta
  },
  {
    name: 'PrintMedicalCertificationOld',
    path: '/print/medcert-old/:visitFormId',
    component: MedCertOld,
    meta
  },
  {
    name: 'PrintMedicalCertification',
    path: '/print/medcert/:visitFormId',
    component: MedCert,
    meta
  },
  {
    name: 'PrintMedicalTransport',
    path: '/print/medical-transport/:patientId',
    component: PrintMedicalTransport,
    meta
  },
  {
    name: 'PrintInvoice',
    path: '/print/invoice/:invoiceId',
    component: Invoice,
    meta
  },
  {
    name: 'PrintReceipt',
    path: '/print/receipt/:invoiceId/:paymentIndex',
    component: Receipt,
    meta
  },
  {
    name: 'PrintOpdCard',
    path: '/print/opd-card/:visitFormId',
    component: OpdCard,
    meta
  },
  {
    name: 'PrintMedicalCertificationBlank',
    path: '/print/medcert-blank/:patientId',
    component: MedCertBlank,
    meta
  },
  {
    name: 'PrintNursingAssessment',
    path: '/print/nursing-assessment/:visitFormId',
    component: NursingAssessment,
    meta
  }
]

const Statistics = () => import('@/components/statistics/statistics')
const StatisticsPatient = () => import('@/components/statistics/patient')

const StatisticsDiscovery = () => import('@/components/statistics/discovery')
const StatisticsDiscoverySummary = () => import('@/components/statistics/discovery/summary')
const StatisticsDiscoveryGrowth = () => import('@/components/statistics/discovery/growth')

// hotel statistics
const StatisticsHotel = () => import('@/components/statistics/hotel')
const StatisticsHotelSummary = () => import('@/components/statistics/hotel/summary')
const StatisticsHotelGrowth = () => import('@/components/statistics/hotel/growth')

const StatisticsAddress = () => import('@/components/statistics/address')
const StatisticsDemographic = () => import('@/components/statistics/demographic')
const StatisticsComparison = () => import('@/components/statistics/comparison')
const StatisticsRegistration = () => import('@/components/statistics/register')
const StatisticsTime = () => import('@/components/statistics/time')

const routes = {
  path: '/statistics',
  component: Statistics,
  children: [
    { path: 'patient', component: StatisticsPatient },
    {
      path: 'discovery',
      component: StatisticsDiscovery,
      children: [
        { path: 'summary', component: StatisticsDiscoverySummary },
        { path: 'growth', component: StatisticsDiscoveryGrowth }
      ],
      redirect: '/statistics/discovery/summary'
    },
    { path: 'comparison', component: StatisticsComparison },
    {
      path: 'hotel',
      component: StatisticsHotel,
      children: [
        { path: 'summary', component: StatisticsHotelSummary },
        { path: 'growth', component: StatisticsHotelGrowth }
      ],
      redirect: '/statistics/hotel/summary'
    },
    { path: 'address', component: StatisticsAddress },
    { path: 'demographic', component: StatisticsDemographic },
    { path: 'time', component: StatisticsTime },
    { path: 'register', component: StatisticsRegistration }
  ],
  redirect: '/statistics/patient'
}

export default routes

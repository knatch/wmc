import ViewPayment from '@/components/payments/index'
const NewPayment = () => import('@/components/payments/new-payment')

const routes = {
  path: '/payments/',
  component: ViewPayment,
  children: [
    { path: 'new', component: NewPayment }
  ]
}

export default routes

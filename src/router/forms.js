import Forms from '@/components/forms/forms'
const MedCerForms = () => import('@/components/forms/medical-certificate')
const OpdCard = () => import('@/components/forms/opd-card')
const NursingIn = () => import('@/components/forms/nursing-intervention')
const NursingAsse = () => import('@/components/forms/nursing-assessment')
const OrderForAcute = () => import('@/components/forms/order-for-acute')
const ProgressNote = () => import('@/components/forms/progress-note')
const PhysicianOrder = () => import('@/components/forms/physician-order')
const DischargeSummary = () => import('@/components/forms/discharge-summary')
const MedicationSheetOneday = () => import('@/components/forms/medication-sheet-oneday')
const MedicationSheetContunue = () => import('@/components/forms/medication-sheet-continue')
const NursingFocusList = () => import('@/components/forms/nursing-focus-list')
const NursingFocusNote = () => import('@/components/forms/nursing-focus-note')
const vitalSign = () => import('@/components/forms/vital-sign')
const nursingAssessmentForm1 = () => import('@/components/forms/nursing-assessment-form1')

const routes = {
  path: '/forms',
  component: Forms,
  children: [
    { path: 'medical-certificate', component: MedCerForms },
    { path: 'opd-card', component: OpdCard },
    { path: 'nursing-intervention', component: NursingIn },
    { path: 'nursing-assessment', component: NursingAsse },
    { path: 'order-for-acute', component: OrderForAcute },
    { path: 'progress-note', component: ProgressNote },
    { path: 'physician-order', component: PhysicianOrder },
    { path: 'discharge-summary', component: DischargeSummary },
    { path: 'medication-sheet-oneday', component: MedicationSheetOneday },
    { path: 'medication-sheet-continue', component: MedicationSheetContunue },
    { path: 'nursing-focus-list', component: NursingFocusList },
    { path: 'nursing-focus-note', component: NursingFocusNote },
    { path: 'vital-sign', component: vitalSign },
    { path: 'nursing-assessment-form1', component: nursingAssessmentForm1 }
  ]
}

export default routes

export default function (to, from, next, store) {
  const authState = store.getters.authState
  const { permission } = authState.data
  const { path } = to

  switch (permission) {
    case 'super-admin':
      next()
      break
    case 'admin':
      next()
      break
    case 'staff':
      if (path.indexOf('statistics') >= 1 || path.indexOf('settings') >= 1) {
        next({ path: '/' })
      } else {
        next()
      }
      break
    case 'analyst': // analyst only allows "/" and "/statistics/"
      if (path === '/' || path.indexOf('statistics') !== -1) {
        next()
      } else {
        next({ path: '/' })
      }
      break

    default:
      // fallback
      next()
      break
  }
}


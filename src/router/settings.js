import Settings from '@/components/settings/settings.vue'
import UserManagement from '@/components/settings/user-management.vue'
import NewUser from '@/components/settings/new-user.vue'
import HotelList from '@/components/settings/hotel-list.vue'
import EditHotel from '@/components/settings/edit-hotel.vue'
import CreateEditHotel from '@/components/settings/create-edit-hotel.vue'
import Zone from '@/components/settings/zone.vue'

const Product = () => import('@/components/products/index')
const ProductMedicine = () => import('@/components/products/medicines')
const ProductLab = () => import('@/components/products/labs')
const ProductSupply = () => import('@/components/products/supplies')

const routes = { path: '/settings',
  component: Settings,
  children: [
    { path: 'user-management', component: UserManagement },
    { path: 'new-user', component: NewUser },
    { path: 'hotel-list', component: HotelList, props: true },
    { path: 'hotel/edit/:hotelId', name: 'EditHotel', component: EditHotel, props: true },
    { path: 'new-hotel', component: CreateEditHotel },
    { path: 'zone', component: Zone },
    { path: 'product',
      component: Product,
      children: [
        { path: 'medicines', component: ProductMedicine },
        { path: 'labs', component: ProductLab },
        { path: 'billable-items', component: ProductSupply }
      ]
    }
  ],
  redirect: '/settings/user-management'
}

export default routes

import ViewInvoices from '@/components/invoices/index'
const NewInvoice = () => import('@/components/invoices/new-invoice')
const Invoices = () => import('@/components/invoices/invoices')
const InvoiceDetail = () => import('@/components/invoices/invoice-detail')

const routes = {
  path: '/invoices',
  component: ViewInvoices,
  children: [
    { path: 'all', component: Invoices },
    { path: 'new', component: NewInvoice },
    { path: ':id', component: InvoiceDetail }
  ],
  redirect: '/invoices/all'
}

export default routes

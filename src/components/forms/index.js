import store from '@/store'

export function precheckFormData (formId, data) {
  const visitId = store.state.visit.visitData.id
  const userId = store.state.auth.data.id

  if (!formId || !visitId || !userId) {
    return false
  }

  const body = {
    visit_id: visitId,
    form_id: formId,
    data: JSON.stringify(data),
    user_id: userId
  }

  return body
}

export function submitFormData (visitFormId, body) {
  return
}

export const FORM_PATH = {
  0: () => import('@/components/forms/404'),
  1: () => import('@/components/forms/medical-certificate'),
  2: () => import('@/components/forms/opd-card'),
  3: () => import('@/components/forms/nursing-intervention'),
  4: () => import('@/components/forms/nursing-assessment'),
  5: () => import('@/components/forms/order-for-acute'),
  6: () => import('@/components/forms/progress-note'),
  7: () => import('@/components/forms/physician-order'),
  8: () => import('@/components/forms/discharge-summary'),
  9: () => import('@/components/forms/medication-sheet-oneday'),
  10: () => import('@/components/forms/medication-sheet-continue'),
  11: () => import('@/components/forms/nursing-focus-list'),
  12: () => import('@/components/forms/nursing-focus-note'),
  13: () => import('@/components/forms/vital-sign'),
  14: () => import('@/components/forms/nursing-assessment-form1'),
  15: () => import('@/components/forms/nursing-assessment-form2'),
  16: () => import('@/components/forms/nursing-assessment-form3'),
  17: () => import('@/components/forms/nursing-assessment-form4'),
  18: () => import('@/components/forms/intravenous-record'),
  19: () => import('@/components/forms/neuro-observ'),
  99: () => import('@/components/invoices/new-invoice')
}

export const FORM_OPD = {
  4: { name: 'Nursing Assessment', order: 1 },
  2: { name: 'OPD Card', order: 2 },
  1: { name: 'Medical Certificate', order: 3 }
}

export const FORM_IPD = {
  8: { name: 'Discharge Summary', order: 1 },
  6: { name: 'Progress Note', order: 2 },
  7: { name: 'Physician Order', order: 3 },
  14: { name: 'Nursing assessment (part 1)', order: 4 },
  15: { name: 'Nursing assessment (part 2)', order: 5 },
  16: { name: 'Nursing assessment (part 3)', order: 6 },
  17: { name: 'Nursing assessment (part 4)', order: 7 },
  9: { name: 'Medication Sheet One Day', order: 8 },
  10: { name: 'Medication Sheet Continue', order: 9 },
  11: { name: 'Nursing Focus List', order: 10 },
  12: { name: 'Nursing Focus Note', order: 11 },
  18: { name: 'Intravenous Record', order: 12 },
  19: { name: 'Neurological Observation Chart', order: 13 }
}

export const FORM_INVOICE = {
  99: { name: 'Create Invoice', order: 1 }
}

export const FORM_OTHER = {
  // 1: 'Medical Certification',
  // 3: 'Nursing Intervention',
  5: { name: 'AGE Order', order: 1 },
  13: { name: 'Vital Sign', order: 2 }
}

export const FORM_REFER = {

}

export const FORM_NAME = {
  1: 'Medical Certification',
  2: 'OPD Card',
  3: 'Nursing Intervention',
  4: 'Nursing Assessment',
  5: 'AGE Order',
  6: 'Progress Note',
  7: 'Physician Order',
  8: 'discharge summary',
  9: 'Medication Sheet One Day',
  10: 'Medication Sheet Continue',
  11: 'Nursing Focus List',
  12: 'Nursing Focus Note',
  13: 'Vital Sign',
  14: 'Nursing assessment (part 1)',
  15: 'Nursing assessment (part 2)',
  16: 'Nursing assessment (part 3)',
  17: 'Nursing assessment (part 4)',
  18: 'Intravenous Record',
  99: 'Invoice'
}

export function amountList (amount) {
  if (!amount) return []

  const _amount = Number(amount)
  const res = []
  let i = 1
  for (i = 1; i <= _amount; i++) {
    res.push(i)
  }

  return res
}

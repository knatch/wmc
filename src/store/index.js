import Vue from 'vue'
import Vuex from 'vuex'

import setAuthToken from '@/api/setAuthToken'
import Cookies from 'js-cookie'

// import auth from '@/store/auth'
import medicalItemModule from './medical-item'

Vue.use(Vuex)

const visitModule = {
  state: {
    visitData: null
  },
  mutations: {
    visitSelection (state, payload) {
      state.visitData = Object.assign({}, payload.data)
    }
  }
}

const store = new Vuex.Store({
  state: {
    patient: null,
    patients: '',
    patientsReported: '',
    patientsReportedCount: '',
    loading: false,
    patientsCount: '',
    auth: {
      status: false,
      data: {}
    }
  },
  getters: {
    loadingState: state => state.loading,
    authState: state => state.auth,
    authData: state => state.auth.data
  },
  actions: {
    LOGIN (context, payload) {
      context.commit('LOGIN', payload)
    }
  },
  mutations: {
    patientSelection (state, payload) {
      state.patient = Object.assign({}, payload.detail)
    },

    patientAddition (state, payload) {
      state.patients.push(payload.detail)
    },

    patientsLoad (state, payload) {
      state.patients = payload.list.filter(patient => patient.deleted_status === false)
    },

    patientsCountLoad (state, payload) {
      state.patientsCount = payload.count
    },

    patientsReportedLoad (state, payload) {
      state.patientsReported = payload.list
    },

    patientsReportedCountLoad (state, payload) {
      state.patientsReportedCount = payload.count
    },

    loadingIncomplete (state, payload) {
      state.loading = payload.detail
    },
    LOGIN (state, payload) {
      state.auth = {
        status: payload.status,
        data: payload.data
      }
      setAuthToken(payload.data.token)
    },
    LOGOUT (state) {
      localStorage.removeItem('wmc-authentication')
      localStorage.removeItem('wmc-authentication-detail')
      Cookies.remove('wmc_token')

      state.auth = {
        status: false,
        data: {}
      }

      // window.reload()
      window.location.reload()
    }
  },
  modules: {
    visit: visitModule,
    medicalItem: medicalItemModule
  }
  /* modules: {
    auth: {
      namespace: true,
      state: auth.state,
      mutations: auth.mutations,
      getters: auth.getters
    }
  } */
})

export default store

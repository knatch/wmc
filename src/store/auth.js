import { LOGIN, LOGOUT } from '@/store/mutation-types'

const state = {
  auth: false
}

const mutations = {
  [LOGIN] (state, data) {
    state.auth = data.auth
  },
  [LOGOUT] (state) {
    state.auth = false
  }
}

const getters = {
  verifyAuth: (state, getters) => {
    return getters.auth
  }
}

export default {
  state,
  mutations,
  getters
}

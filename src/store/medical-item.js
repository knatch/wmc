const medicalItemModule = {
  state: {
    billableItemData: [],
    productData: [],
    labData: []
  },
  mutations: {
    loadBillableItem (state, payload) {
      state.billableItemData = payload
    },
    loadProduct (state, payload) {
      state.productData = payload
    },
    loadLab (state, payload) {
      state.labData = payload
    }
  }
}

export default medicalItemModule

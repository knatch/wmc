export function printTable (page) {
  let css = `@page { size: ${page}; font-size: 14px; }`
  if (page === 'landscape') css += ` .section-to-print { writing-mode: rb-tl; }`
  const head = document.head || document.getElementsByTagName('head')[0]
  const style = document.createElement('style')

  style.type = 'text/css'
  style.media = 'print'

  if (style.styleSheet) {
    style.styleSheet.cssText = css
  } else {
    style.appendChild(document.createTextNode(css))
  }

  head.appendChild(style)
  // print function
  window.print()
  head.removeChild(head.lastChild)
}

export const MONTHS = {
  1: 'JAN',
  2: 'FEB',
  3: 'MAR',
  4: 'APR',
  5: 'MAY',
  6: 'JUN',
  7: 'JUL',
  8: 'AUG',
  9: 'SEP',
  10: 'OCT',
  11: 'NOV',
  12: 'DEC'
}

export const COLORS = [
  'rgb(55, 172, 227)', '#EC2734', 'rgb(75, 192, 192)', '#F8F991', 'rgb(255, 159, 64)', 'rgb(153, 102, 255)', 'rgb(255, 99, 132)', 'rgb(255, 205, 86)', '#FF1744', '#1B503E', '#1DE986', '#FFC400', '#D17B07', '#B7F0AD', '#BAAD22', '#444B6E'
]

export const YEARS = () => {
  let startYear = 2016
  const current = new Date()
  const currentYear = current.getFullYear()
  const years = []
  while (currentYear >= startYear) years.push(startYear++)

  return years
}

export function generateColor () {
  return '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
}

export const discoveryList = ['walk pass', 'hotel', 'pharmacy', 'google', 'friend', 'hospital', 'street', 'local', 'brochure', 'boatman', 'tour guide', 'bar', 'restaurant', 'insurance', 'other']

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'

// filter pipe ( | )
import Filters from './filters'

// enable calling this.$lodash and $lodash in vue file
import lodash from 'lodash'

// vue-fontawesome
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'

// enable calling this.moment and moment in vue file
import moment from 'moment'

// vue meta for custom css for some route, component
import VueMeta from 'vue-meta'

import Cookies from 'js-cookie'
import setAuthToken from '@/api/setAuthToken'
import { authVerify } from '@/api/auth'
import navigation from './router/navigation'

import { getBillableItems } from '@/api/billable-item'
import { getProducts, getLabs } from '@/api/product'

// the one with imported css
import 'flatpickr/dist/flatpickr.min.css'
import 'flexboxgrid/dist/flexboxgrid.min.css'
import 'slim-select/dist/slimselect.min.css'

// use this.$lodash as a global object
Object.defineProperty(Vue.prototype, '$lodash', { value: lodash })
Vue.prototype.moment = moment

Vue.component('icon', Icon)
Vue.config.productionTip = false

Vue.use(VueMeta)
Vue.use(require('vue-moment'))

Vue.filter('age', Filters.age)
Vue.filter('numberWithCommas', Filters.numberWithCommas)

store.commit('loadingIncomplete', { detail: true })
const token = Cookies.get('wmc_token')

if (token) {
  // set axios header token
  setAuthToken(token)

  try {
    authVerify(token).then(result => {
      if (result && result.status === 200) {
        const data = result.data

        store.dispatch('LOGIN', { status: true, data: data }).then(() => {
          // after state is set, assign navigation guard
          router.beforeEach((to, from, next) => {
            navigation(to, from, next, store)
          })
        }).finally(() => {
          initApp()
          Promise.all([
            getBillableItems(),
            getProducts(),
            getLabs()
          ])
          // return getBillableItems().then(data => {
            // store.commit('loadBillableItem', data)
          // })
        })
      }
      store.commit('loadingIncomplete', { detail: false })
    })
  } catch (err) {
    // error occurs, commit logout and reload
    store.commit('loadingIncomplete', { detail: false })
    router.push('/login')

    store.commit('LOGOUT')
  }
} else {
  // no token presents
  // redirect to /login page
  router.push('/login')
  router.beforeEach((to, from, next) => {
    navigation(to, from, next, store)
  })

  store.commit('loadingIncomplete', { detail: false })
  initApp()
}

function initApp () {
  // console.info('init new app')
  /* eslint-disable no-new */
  new Vue({
    el: '#app',
    store,
    router,
    template: '<App/>',
    components: { App }
  })
}

import Axios from 'axios'
import { handleLoadingErrors } from './error'
import cacheBuster from './cacheBuster'

const URL = process.env.API_URL + `/visit-forms`
const headers = {
  'Content-Type': 'application/json'
}

export function createVisitForm (body) {
  const method = 'POST'
  const data = {
    visit_id: body.visit_id,
    form_id: body.form_id,
    data: body.data,
    user_id: body.user_id
  }

  return Axios({method, url: URL, headers, data}).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
  })
}

export function getVisitForms (visitId) {
  const url = URL
  const params = {
    visit_id: visitId
  }

  return Axios.get(url, {headers, params}).then(response => response).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function getVisitFormById (id) {
  const url = URL + '/' + id
  const uncachedUrl = cacheBuster(url)

  return Axios.get(uncachedUrl, {headers}).then(response => response).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function deleteVisitForm (id) {
  const method = 'DELETE'
  const url = URL + '/' + id

  return Axios({method, url, headers}).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
  })
}

export function editVisitForm (id, body) {
  const method = 'PUT'
  const url = URL + '/' + id
  const data = {
    data: body.data,
    user_id: body.user_id
  }

  return Axios({method, url, headers, data}).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
  })
}

export function approveVisitForm (id) {
  const method = 'PUT'
  const url = URL + '/' + id + '/approve'

  return Axios({method, url, headers}).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
  })
}

export function addProcedureToVisitForm (id, data) {
  const method = 'PUT'
  const url = URL + '/' + id + '/procedure'

  return Axios({method, url, headers, data}).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
  })
}

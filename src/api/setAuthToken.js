import axios from 'axios'
import API from './index'

export default function setAuthToken (token) {
  axios.defaults.headers.common['wmc-authorization'] = ''
  delete axios.defaults.headers.common['wmc-authorization']

  if (token) {
    axios.defaults.headers.common['wmc-authorization'] = `${token}`
    API.setAuthToken(token)
  }
}

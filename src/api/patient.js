import STORE from '../store'
import Axios from 'axios'
import { handleLoadingErrors } from './error'
import setAuthToken from './setAuthToken'

const URL = process.env.API_URL
const headers = {
  'Content-Type': 'application/json'
}

const token = localStorage.getItem('wmc-authentication')
setAuthToken(token)

export function getPatients (params = {}) {
  STORE.state.loading = true

  const method = 'GET'
  const url = URL + '/patients'
  const _params = {
    limit: parseInt(params.limit) || 50,
    page: parseInt(params.page) || null,
    branch_id: params.branch_id || null,
    deleted_status: params.deleted_status || null
  }

  return Axios({
    method,
    url,
    headers,
    params: _params
  }).then(response => {
    STORE.state.loading = false
    return { list: response.data.rows, count: response.data.count }
  }).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function searchPatients (keyword) {
  const method = 'GET'
  const url = URL + '/patients/search'
  const _params = {
    keyword: keyword
  }

  return Axios({
    method,
    url,
    headers,
    params: _params
  }).then(response => {
    return response.data
  }).catch(error => {
    return handleLoadingErrors(error)
  })
}

// hotel_id here
export function bulkUpdatePatients (data) {
  const method = 'PUT'
  const url = URL + '/patients/bulkUpdate'

  return Axios({
    method,
    url,
    headers,
    data
  }).then(response => {
    return response.data
  }).catch(error => {
    return handleLoadingErrors(error)
  })
}

// reject patient (make deleted_status true)
export function rejectPatient (id) {
  STORE.state.loading = true
  const method = 'DELETE'
  const url = URL + '/patients/delete/' + id

  return Axios({
    method,
    url,
    headers
  }).then(response => {
    STORE.state.loading = false
    return response.data
  }).catch(error => {
    return handleLoadingErrors(error)
  })
}

import STORE from '../store'

export function handleLoadingErrors (error) {
  let errMsg = 'Something went wrong. Please contact your administrator'
  console.error('ajax call error', error)

  if (error.response && error.response.data) {
    // console.log(error.response.data)
    // console.log(error.response.status)
    // console.log(error.response.headers)
    if (typeof error.response.data === 'string') errMsg = error.response.data
    if (typeof error.response.data === 'object' && error.response.data.message) errMsg = error.response.data.message
  }

  alert(errMsg)
  STORE.state.loading = false

  // call logout if error message is failed to authenticate token
  if (errMsg === 'Failed to authenticate token.') {
    STORE.commit('LOGOUT')
  }
}

import Axios from 'axios'
import { handleLoadingErrors } from './error'

const URL = process.env.API_URL
const headers = {
  'Content-Type': 'application/json'
}

export function fetchIcd (keyword) {
  const url = URL + `/icd`
  const params = {
    keyword
  }

  return Axios.get(url, {headers, params}).then(response => {
    return response.data
  }).catch(error => handleLoadingErrors(error))
}

export const icdEndpoint = URL + '/icd'

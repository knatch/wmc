import Axios from 'axios'
import { handleLoadingErrors } from './error'
import store from '@/store'

const URL = process.env.API_URL
const headers = {
  'Content-Type': 'application/json'
}

export function getBillableItems () {
  const url = URL + `/billable-items`

  return Axios.get(url, {headers}).then(response => {
    store.commit('loadBillableItem', response.data)
  }).catch(error => handleLoadingErrors(error))
}

import Axios from 'axios'
import { handleLoadingErrors } from './error'
import store from '@/store'

const URL = process.env.API_URL
const headers = {
  'Content-Type': 'application/json'
}

export function getProducts () {
  const url = URL + `/products`

  return Axios.get(url, {headers}).then(response => {
    store.commit('loadProduct', response.data)
  }).catch(error => handleLoadingErrors(error))
}

export function getLabs () {
  const url = URL + `/labs`

  return Axios.get(url, {headers}).then(response => {
    store.commit('loadLab', response.data)
  }).catch(error => handleLoadingErrors(error))
}


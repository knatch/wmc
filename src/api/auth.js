import Axios from 'axios'

Axios.defaults.withCredentials = true

const URL = process.env.API_URL
const headers = {
  'Content-Type': 'application/json'
}

export function authLogin (data = {}) {
  const method = 'POST'
  const url = URL + '/auth'
  const _data = {
    username: data.username,
    password: data.password
  }

  return Axios({
    method,
    url,
    headers,
    data: _data
  }).then(response => {
    // STORE.state.loading = false
    return response
  }).catch(error => {
    return error.response
    // return handleLoadingErrors(error)
  })
}

export function authVerify (token) {
  return Axios({
    method: 'POST',
    url: URL + '/auth/verify',
    data: { token },
    headers
  }).then(response => response)
    .catch(error => error)
}

import Axios from 'axios'
import { handleLoadingErrors } from './error'

const URL = process.env.API_URL
const headers = {
  'Content-Type': 'application/json'
}

export function getRole () {
  const url = URL + '/role'

  return Axios.get(url, {headers}).then(response => response).catch(error => {
    return handleLoadingErrors(error)
  })
}

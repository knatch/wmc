export default function cacheBuster (url) {
  const current = new Date().getTime()

  return `${url}?_=${current}`
}


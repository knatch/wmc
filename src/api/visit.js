import Axios from 'axios'
import { handleLoadingErrors } from './error'

const URL = process.env.API_URL
const headers = {
  'Content-Type': 'application/json'
}

export function getPatientVisitsById (patientId) {
  const url = URL + `/visits`
  const params = {
    patient_id: patientId
  }

  return Axios.get(url, {headers, params}).then(response => response).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function getVisitById (visitId) {
  const url = URL + `/visits/${visitId}`

  return Axios.get(url, headers).then(response => response.data).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function createPatientVisit (patientId, branchId) {
  const method = 'POST'
  const url = URL + `/visits`
  const data = {
    patient_id: patientId,
    branch_id: branchId
  }

  return Axios({method, url, headers, data}).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
  })
}

export function getPendingVisit () {
  const url = URL + '/visits/pending'

  return Axios.get(url, {headers}).then(response => response).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function getMedCerInfo (visitId) {
  const url = URL + `/visits/${visitId}/medical-certificate`

  return Axios.get(url, headers).then(response => response.data).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function getOpdCardInfo (visitId) {
  const url = URL + `/visits/${visitId}/opd-card`

  return Axios.get(url, headers).then(response => response.data).catch(error => {
    return handleLoadingErrors(error)
  })
}

import STORE from '../store'
import Axios from 'axios'
import { handleLoadingErrors } from './error'

const URL = process.env.API_URL
const headers = {
  'Content-Type': 'application/json'
}

export function getStatisticsPatients () {
  const method = 'GET'
  const url = URL + '/statistics/patients'

  return Axios({
    method,
    url,
    headers
  }).then(response => {
    STORE.state.loading = false
    return response
  }).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function getStatisticsPatientsDiscovery (year = null) {
  const method = 'GET'
  const url = URL + '/statistics/patients/discovery'
  const params = {
    year: year === 'all' ? null : year
  }

  return Axios({
    method,
    url,
    headers,
    params
  }).then(response => {
    STORE.state.loading = false
    return response
  }).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function getStatisticsPatientsDiscoveryByMonth (month, year) {
  const method = 'GET'
  const url = URL + '/statistics/patients/discovery/byMonth'
  const params = {
    month,
    year
  }

  return Axios({
    method,
    url,
    headers,
    params
  }).then(response => {
    STORE.state.loading = false
    return response
  }).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function getStatisticsPatientsByMonth (month, year) {
  const method = 'GET'
  const url = URL + '/statistics/patients/byMonth'
  const _params = {
    month,
    year
  }

  return Axios({
    method,
    url,
    headers,
    params: _params
  }).then(response => {
    STORE.state.loading = false
    return response
  }).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function getStatisticsPatientsByTime (startDate, endDate) {
  const method = 'POST'
  const url = URL + '/statistics/patients/byTime'
  const _params = {
    startDate, endDate
  }

  return Axios({
    method,
    url,
    headers,
    params: _params
  }).then(response => {
    STORE.state.loading = false
    return response
  }).catch(error => {
    return handleLoadingErrors(error)
  })
}

// referenced in components/statistics/discovery/growth.vue
export function getStatisticsPatientsDiscoveryGrowth (startMonth, startYear, endMonth, endYear) {
  const url = URL + '/statistics/patients/discovery/growth'
  const _params = {
    startMonth,
    startYear,
    endMonth,
    endYear
  }

  return Axios.get(url, {headers, params: _params}).then(response => response).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function getStatisticsPatientsHotelSummary (month, year) {
  const url = URL + '/statistics/patients/hotel'
  const _params = {
    month,
    year
  }

  return Axios.get(url, {headers, params: _params}).then(response => response).catch(error => {
    return handleLoadingErrors(error)
  })
}

// referenced in components/statistics/hotel/growth.vue
export function getStatisticsPatientsHotelZone (zone, month, year, endMonth, endYear) {
  const url = URL + '/statistics/patients/hotel/zone'
  const _params = {
    month,
    year,
    zone,
    endMonth,
    endYear
  }

  return Axios.get(url, {headers, params: _params}).then(response => response).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function getComparisonStatistics (obj) {
  const method = 'POST'
  const url = URL + '/statistics/comparison'
  const _param = obj

  return Axios({
    method,
    url,
    headers,
    params: _param
  }).then(response => {
    return response
  }).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function getStatisticsDemographic (month, year) {
  const url = URL + '/statistics/patients/demographic'
  const _params = {
    month,
    year
  }

  return Axios.get(url, {headers, params: _params}).then(response => response).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function getStatisticsRegistration (start, end) {
  const url = URL + '/statistics/patients/registration'
  const _params = { start, end }

  return Axios.get(url, {headers, params: _params}).then(response => response).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function getStatisticsTime (start, end) {
  const url = URL + '/statistics/patients/length'
  const _params = { start, end }

  return Axios.get(url, {headers, params: _params}).then(response => response).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function getStatisticsAddress (start, end) {
  const url = URL + '/statistics/patients/address'
  const _params = { start, end }

  return Axios.get(url, {headers, params: _params}).then(response => response).catch(error => {
    return handleLoadingErrors(error)
  })
}

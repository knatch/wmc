import STORE from '../store'
import 'whatwg-fetch'

const URL = process.env.API_URL
const headers = {
  'Content-Type': 'application/json'
}

const token = localStorage.getItem('wmc-authentication')
headers['wmc-authorization'] = token

class API {
  static url = `${URL}`
  static assetUrl = `${URL}/assets/`

  static handleErrors (response) {
    if (!response.ok) throw Error(response.statusText)
    return response.json()
  }

  static handleLoadingErrors (error) {
    alert('Something went wrong. Please contact your administrator')
    STORE.state.loading = false
    console.log(error)
  }

  static getPatients () {
    STORE.state.loading = true
    const url = this.url + '/patients'
    return fetch(url, { headers: headers })
      .then(response => this.handleErrors(response))
      .then(data => {
        STORE.state.loading = false
        return data
      })
      .catch(error => this.handleLoadingErrors(error))
  }

  static getPatientById (id) {
    STORE.state.loading = true
    const url = this.url + '/patients/' + id
    return fetch(url, { headers: headers })
      .then(response => this.handleErrors(response))
      .then(data => {
        STORE.state.loading = false
        return data
      })
      .catch(error => this.handleLoadingErrors(error))
  }

  static getPatientByHN (hn) {
    STORE.state.loading = true
    const url = this.url + '/patients?hospital_number_display=' + hn
    return fetch(url, { headers: headers })
      .then(response => this.handleErrors(response))
      .then(data => {
        STORE.state.loading = false
        return data
      })
      .catch(error => this.handleLoadingErrors(error))
  }

  static addPatient (body) {
    STORE.state.loading = true
    const url = this.url + '/patients'
    Object.keys(body).forEach(key => body[key] === '' && delete body[key])
    const Body = {
      method: 'POST',
      body: JSON.stringify(body),
      headers: headers
    }
    return fetch(url, Body)
      .then(response => this.handleErrors(response))
      .then(data => {
        STORE.state.loading = false
        return data
      })
      .catch(error => this.handleLoadingErrors(error))
  }

  static editPatient (body, id) {
    STORE.state.loading = true
    const url = this.url + '/patients/' + id
    const Body = {
      method: 'PUT',
      body: JSON.stringify(body),
      headers: headers
    }
    return fetch(url, Body)
      .then(response => {
        return this.handleErrors(response)
      })
      .then(data => {
        STORE.state.loading = false
        return data
      })
      .catch(error => this.handleLoadingErrors(error))
  }

  static rejectPatient (id) {
    STORE.state.loading = true
    const url = this.url + '/patients/delete/' + id
    return fetch(url, {method: 'DELETE', headers: headers})
      .then(response => {
        return this.handleErrors(response)
      })
      .then(data => {
        STORE.state.loading = false
        return data
      })
      .catch(error => this.handleLoadingErrors(error))
  }

  static deletePatient (id) {
    STORE.state.loading = true
    const url = this.url + '/patients/' + id
    return fetch(url, {method: 'DELETE', headers: headers})
      .then(response => {
        return this.handleErrors(response)
      })
      .then(data => {
        STORE.state.loading = false
        return data
      })
      .catch(error => this.handleLoadingErrors(error))
  }

  static undoRejectPatient (id) {
    STORE.state.loading = true
    const url = this.url + '/patients/' + id
    const Body = {
      method: 'PUT',
      body: JSON.stringify({deleted_status: false}),
      headers: headers
    }
    return fetch(url, Body)
      .then(response => {
        return this.handleErrors(response)
      })
      .then(data => {
        STORE.state.loading = false
        return data
      })
      .catch(error => this.handleLoadingErrors(error))
  }

  static setAuthToken (token) {
    headers['wmc-authorization'] = token
  }
}

export default API

import Axios from 'axios'
import { handleLoadingErrors } from './error'

const URL = process.env.API_URL
const headers = {
  'Content-Type': 'application/json'
}

export function getInvoices () {
  const url = URL + `/invoices`

  return Axios.get(url, {headers}).then(response => response).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function getInvoicesByVisitId (visitId) {
  const url = URL + `/invoices?visit_id=${visitId}`

  return Axios.get(url, {headers}).then(response => response).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function createInvoice (body) {
  const method = 'POST'
  const url = URL + `/invoices`
  const data = body

  return Axios({method, url, headers, data}).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
  })
}

export function getInvoiceById (id) {
  const url = URL + `/invoices/${id}`

  return Axios.get(url, {headers}).then(response => response).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function deleteInvoiceById (id) {
  const url = URL + `/invoices/${id}`
  const method = 'DELETE'

  return Axios({method, url, headers}).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
  })
}

import Axios from 'axios'
import { handleLoadingErrors } from './error'

const URL = process.env.API_URL + '/payments'
const headers = {
  'Content-Type': 'application/json'
}

export function createPayment (body) {
  const url = URL
  const method = 'POST'
  const data = body

  return Axios({method, url, headers, data}).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
  })
}

export function deletePaymentById (id) {
  const url = URL + `/${id}`
  const method = 'DELETE'

  return Axios({method, url, headers}).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
  })
}

export function getPayments () {
  const url = `${URL}`
  const method = 'GET'

  return Axios({ method, url, headers }).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
  })
}

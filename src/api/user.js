import Axios from 'axios'
import { handleLoadingErrors } from './error'

const URL = process.env.API_URL + '/users'
const headers = {
  'Content-Type': 'application/json'
}

export function getUsers (params = {}) {
  const method = 'GET'

  return Axios({
    method,
    url: URL,
    headers,
    params
  }).then(response => {
    return response
  }).catch(error => {
    return handleLoadingErrors(error)
  })
}

export function createUser (body) {
  const method = 'POST'
  const data = {
    first_name: body.first_name,
    last_name: body.last_name,
    username: body.username,
    password: body.password,
    role_id: body.role_id,
    created_by: body.created_by
  }

  return Axios({
    method,
    url: URL,
    headers,
    data
  }).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
    return error
  })
}

export function removeUser (username) {
  const method = 'DELETE'
  const url = URL + `/${username}`

  return Axios({
    method,
    url,
    headers
  }).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
    return error
  })
}

export function updateUser (data) {
  const method = 'PUT'
  const url = URL + `/${data.username}`

  return Axios({
    method,
    url,
    headers,
    data
  }).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
    return error
  })
}

import Axios from 'axios'
import { handleLoadingErrors } from './error'

const URL = process.env.API_URL + '/hotels'
const headers = {
  'Content-Type': 'application/json'
}

function sendRequest (metadata) {
  return Axios(metadata).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
    return error
  })
}

export function getHotels () {
  return Axios({method: 'GET', url: URL, headers})
    .then(response => {
      return response
    }).catch(error => {
      return handleLoadingErrors(error)
    })
}

export function createHotel (data) {
  const method = 'POST'
  const _data = {
    hotel_name: data.hotel_name,
    hotel_zone_id: data.hotel_zone_id
  }

  return Axios({
    method,
    url: URL,
    headers,
    data: _data
  }).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
    return error
  })
}

export function editHotel (id, data) {
  const method = 'PUT'
  const url = URL + '/' + id
  const _data = {
    hotel_name: data.hotel_name,
    hotel_zone_id: data.hotel_zone_id
  }

  return Axios({
    method,
    url,
    headers,
    data: _data
  }).then(response => {
    return response
  }).catch(error => {
    handleLoadingErrors(error)
    return error
  })
}

export function removeHotel (id) {
  const method = 'DELETE'
  const url = URL + '/' + id

  return sendRequest({
    method,
    url,
    headers,
    withCredentials: true
  })
}

// TODO: move this to zone.js
export function getHotelZone () {
  return Axios({method: 'GET', url: URL + '/zone', headers, params: {_: new Date()}})
    .then(response => {
      return response
    }).catch(error => {
      return handleLoadingErrors(error)
    })
}

export function editZone (data) {
  const method = 'PUT'
  const url = URL + '/zone'
  const _data = {
    name: data.name,
    id: data.id
  }

  return sendRequest({method, url, headers, data: _data})
}

export function createZone (data) {
  const method = 'POST'
  const url = URL + '/zone'
  const _data = {
    name: data.name
  }

  return sendRequest({method, url, headers, data: _data})
}

export function removeZone (id) {
  const method = 'DELETE'
  const url = URL + '/zone/' + id

  return sendRequest({
    method,
    url,
    headers,
    withCredentials: true
  })
}

var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_URL: '"http://localhost:8080/api"',
  ASSET_URL: '"https://worldmed.hospital/api/assets/"',
  PRINT_URL: '"http://localhost:8080/api/p"'
})

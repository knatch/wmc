var moment = require('moment')
var buildDate = moment().format('DD-MM-YY')

module.exports = {
  NODE_ENV: '"production"',
  BUILD_DATE: JSON.stringify(buildDate),
  // API_URL: '"https://worldmed.hospital/api"',
  API_URL: '"https://admin-staging.worldmed.hospital/api"',
  ASSET_URL: '"https://worldmed.hospital/api/assets/"',
  PRINT_URL: '"https://worldmed.hospital/api/p"',
}

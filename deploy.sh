ssh-add ~/.ssh/natchy_id_rsa
rm -rf dist
npm run build
zip -r dist.zip dist
# scp dist.zip web@worldmed.hospital:/var/www/worldmed.hospital/html
# scp dist.zip knatch@worldmed.hospital:~/server/public
scp dist.zip knatch@worldmed.hospital:~

# ssh -A knatch@worldmed.hospital "cd /mnt/server/public
# sudo mv ~/dist.zip .
# sudo rm -rf static index.html
# sudo unzip dist.zip
# sudo mv dist/* .
# sudo rm -rf dist*
# "
